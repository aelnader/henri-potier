import { configure } from '@storybook/react';

function loadStories() {
  require('../src/js/stories/card.stories.js');
  require('../src/js/stories/badge.stories.js');
  // You can require as many stories as you need.
}

configure(loadStories, module);
