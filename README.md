
# The Potter Shop

This is an online bookshop for selling the Harry Potter books.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

If you don't have yarn installed on your machine:

```
npm install -g yarn
```


### Installing

Install npm dependencies for project:

```
yarn install
```

To run the project on your local machine: 

```
yarn start
```
By default, webpack-dev-server will run on http://localhost:8080.

## Running the tests

To run unit tests: 

```
yarn test
```
To run storybook:

```
yarn storybook
```
By default, storybook will run on http://localhost:9001.
## Deployment

```
yarn build
```
Will generate a bundle in ./dist folder ready to be deployed on server.
## Built With

* [React.js](https://reactjs.org/) - The web framework used
* [Redux](https://redux.js.org/) - State Management
* [Redux Saga](https://github.com/redux-saga/redux-saga) - To manage side-effects
* [Jest](https://facebook.github.io/jest/) - Testing
* [Enzyme](https://github.com/airbnb/enzyme) - Testing utilities for React 
* [Google's MDL](https://getmdl.io/) - CSS Framework
## Authors

* **Alaa EL NADERI**

## License

This project is licensed under the MIT License
