import { call, put } from 'redux-saga/effects';
// actions
import { booksSucceeded, booksFailed } from '../actions/books';
// constants
import { GET_BOOKS_URL } from '../constants/api';

export function fetchBooks() {
  return fetch(GET_BOOKS_URL).then(response => response.json());
}
/**
 * Handles books data fetch and synchronization with the application
 */
export function* booksSaga() {
  try {
    const books = yield call(fetchBooks);
    yield put(booksSucceeded(books));
  } catch (e) {
    yield put(booksFailed(e));
  }
}
