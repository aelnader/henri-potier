import { call, put } from 'redux-saga/effects';
// actions
import { offersSucceeded, offersFailed } from '../actions/cart';
// constants
import { GET_BOOKS_URL, GET_OFFERS } from '../constants/api';

/**
 * Makes GET call to fetch offers
 * @param  {array} itemsWithPrices array of [{isbn, quantity}...]
 * @return {object}                 fetch offers result
 */
export function fetchOffers(itemsWithPrices) {
  let url = `${GET_BOOKS_URL}/`;
  for (let i = 0; i < itemsWithPrices.length; i += 1) {
    for (let j = 0; j < itemsWithPrices[i].quantity; j += 1) {
      url += `${itemsWithPrices[i].isbn},`;
    }
  }
  url = url.slice(0, -1) + GET_OFFERS;
  return fetch(url).then(response => response.json());
}

/**
 * [calculateBestOffer description]
 * @param  {object} offersResult    fetch offers result
 * @param  {array} itemsWithPrices array of [{isbn, quantity}...]
 * @return {number}                 Best possible offer
 */
function calculateBestOffer(offersResult, itemsWithPrices) {
  let totalPrice = 0;
  let bestOffer = 0;
  for (let i = 0; i < itemsWithPrices.length; i += 1) {
    totalPrice += (itemsWithPrices[i].price * itemsWithPrices[i].quantity);
  }
  const percentageOffer = offersResult.offers.find(element => element.type === 'percentage');
  if (percentageOffer) {
    // calculate would be reduction in euro
    const percentageOfferReduction = (totalPrice * percentageOffer.value) / 100;
    bestOffer = percentageOfferReduction;
  }
  const minusOffer = offersResult.offers.find(element => element.type === 'minus');
  if (minusOffer && bestOffer < minusOffer.value) {
    bestOffer = minusOffer.value;
  }
  const sliceOffer = offersResult.offers.find(element => element.type === 'slice');
  if (sliceOffer && totalPrice >= sliceOffer.sliceValue && bestOffer < sliceOffer.value) {
    bestOffer = sliceOffer.value;
  }
  return bestOffer;
}
/**
 * Handles data fetch offers and promotions and syncs them with the application
 */
export function* offersSaga(action) {
  const itemsWithPrices = action.payload;
  try {
    const offersResult = yield call(fetchOffers, itemsWithPrices);
    const bestOffer = calculateBestOffer(offersResult, itemsWithPrices);
    yield put(offersSucceeded({
      bestOffer,
    }));
  } catch (e) {
    yield put(offersFailed(e));
  }
}
