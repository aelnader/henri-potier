import { call, put } from 'redux-saga/effects';
import { cloneableGenerator } from 'redux-saga/utils';

import { offersSaga, fetchOffers } from '../OffersSaga';
import { offersSucceeded, offersFailed } from '../../actions/cart';
import * as actionTypes from '../../constants/actionTypes';

describe('OffersSaga', () => {
  const mockAction = {
    type: actionTypes.OFFERS_REQUESTED,
    payload: [
      {
        isbn: 'c8fabf68-8374-48fe-a7ea-a00ccd07afff',
        price: 30,
        quantity: 1,
      },
      {
        isbn: 'c30968db-cb1d-442e-ad0f-80e37c077f89',
        price: 35,
        quantity: 1,
      },
    ],
  };
  const offersSagaGen = cloneableGenerator(offersSaga)(mockAction);
  it('should try to fetch the offers data', () => {
    expect(offersSagaGen.next().value).toEqual(call(fetchOffers, mockAction.payload));
  });
  it('should call an action to signal success and store data in case off success', () => {
    const offersSagaGenClone = offersSagaGen.clone();
    const offersResult = {
      offers: [
        {
          type: 'percentage',
          value: 5,
        },
        {
          type: 'minus',
          value: 15,
        },
        {
          type: 'slice',
          sliceValue: 100,
          value: 12,
        },
      ],
    };
    expect(offersSagaGenClone.next(offersResult).value)
      .toEqual(put(offersSucceeded({ bestOffer: 15 })));
  });
  it('should call an action to signal error in case of error', () => {
    const booksSagaGenClone = offersSagaGen.clone();
    const error = {
      code: '404',
      message: 'Page not found',
    };
    expect(booksSagaGenClone.throw(error).value)
      .toEqual(put(offersFailed(error)));
  });
  it('should send percentage offer value if it\'s the best offer', () => {
    const offersSagaGenClone = offersSagaGen.clone();
    const offersResult = {
      offers: [
        {
          type: 'percentage',
          value: 50,
        },
        {
          type: 'minus',
          value: 15,
        },
        {
          type: 'slice',
          sliceValue: 100,
          value: 12,
        },
      ],
    };
    expect(offersSagaGenClone.next(offersResult).value)
      .toEqual(put(offersSucceeded({ bestOffer: 32.5 })));
  });
  it('should send minus offer value if it\'s the best offer', () => {
    const offersSagaGenClone = offersSagaGen.clone();
    const offersResult = {
      offers: [
        {
          type: 'percentage',
          value: 5,
        },
        {
          type: 'minus',
          value: 15,
        },
        {
          type: 'slice',
          sliceValue: 100,
          value: 12,
        },
      ],
    };
    expect(offersSagaGenClone.next(offersResult).value)
      .toEqual(put(offersSucceeded({ bestOffer: 15 })));
  });
  it('should send slice offer value if it\'s the best offer', () => {
    const offersSagaGenClone = offersSagaGen.clone();
    const offersResult = {
      offers: [
        {
          type: 'percentage',
          value: 5,
        },
        {
          type: 'minus',
          value: 15,
        },
        {
          type: 'slice',
          sliceValue: 50,
          value: 20,
        },
      ],
    };
    expect(offersSagaGenClone.next(offersResult).value)
      .toEqual(put(offersSucceeded({ bestOffer: 20 })));
  });
});
