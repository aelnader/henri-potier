import { call, put } from 'redux-saga/effects';
import { cloneableGenerator } from 'redux-saga/utils';

import { booksSaga, fetchBooks } from '../BooksSaga';
import { booksSucceeded, booksFailed } from '../../actions/books';

describe('BooksSaga', () => {
  const booksSagaGen = cloneableGenerator(booksSaga)();
  it('should try to fetch the books data', () => {
    expect(booksSagaGen.next().value).toEqual(call(fetchBooks));
  });
  it('should call an action to signal success and store data in case off success', () => {
    const booksSagaGenClone = booksSagaGen.clone();
    const booksResult = [
      {
        isbn: 'c8fabf68-8374-48fe-a7ea-a00ccd07afff',
        title: "Henri Potier à l'école des sorciers",
        price: 35,
        cover: 'http://henri-potier.xebia.fr/hp0.jpg',
        synopsis: ['synopsis'],
      },
    ];
    expect(booksSagaGenClone.next(booksResult).value)
      .toEqual(put(booksSucceeded(booksResult)));
  });
  it('should call an action to signal error in case of error', () => {
    const booksSagaGenClone = booksSagaGen.clone();
    const error = {
      code: '404',
      message: 'Page not found',
    };
    expect(booksSagaGenClone.throw(error).value)
      .toEqual(put(booksFailed(error)));
  });
});
