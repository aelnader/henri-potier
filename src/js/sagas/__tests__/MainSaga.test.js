import { takeLatest } from 'redux-saga/effects';
import mainSaga from '../MainSaga';
import { booksSaga } from '../BooksSaga';
import { offersSaga } from '../OffersSaga';
import * as actionTypes from '../../constants/actionTypes';

describe('MainSaga', () => {
  const mainSagaGen = mainSaga();
  it('should run the booksSaga each time BOOKS_REQUESTED action is dispatched', () => {
    expect(mainSagaGen.next().value)
      .toEqual(takeLatest(actionTypes.BOOKS_REQUESTED, booksSaga));
  });
  it('should run the promotionsSaga each time PROMOTION_REQUESTED action is dispatched', () => {
    expect(mainSagaGen.next().value)
      .toEqual(takeLatest(actionTypes.OFFERS_REQUESTED, offersSaga));
  });
});
