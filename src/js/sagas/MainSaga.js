import { takeLatest } from 'redux-saga/effects';
// sagas
import { booksSaga } from './BooksSaga';
import { offersSaga } from './OffersSaga';
// constants
import { BOOKS_REQUESTED, OFFERS_REQUESTED } from '../constants/actionTypes';
/**
 * Main saga which runs all sagas handling the application's side effects
 */
export default function* mainSaga() {
  // Initiate sagas here
  yield takeLatest(BOOKS_REQUESTED, booksSaga);
  yield takeLatest(OFFERS_REQUESTED, offersSaga);
}
