/**
 * This file contains string constants for all wordings(msgs, labels, titles etc.)
 */

// Layout
export const MAIN_TITLE = 'The Potter Shop';
export const PAGE_BOOKS_TITLE = 'Livres';
export const PAGE_CART_TITLE = 'Panier';

// Labels
export const ADD_TO_CART_BUTTON = 'Ajouter au panier';

// erros msgs
export const FETCH_BOOKS_ERROR_MSG = 'Could not show book collection at the moment, please come back later or try refreshing your page';

// msgs
export const CART_EMPTY_MSG = 'Votre panier est vide';
