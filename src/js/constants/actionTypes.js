/**
 * This file contains string constants for action types
 */

// books
export const BOOKS_REQUESTED = 'BOOKS_REQUESTED';
export const BOOKS_SUCCEEDED = 'BOOKS_SUCCEEDED';
export const BOOKS_FAILED = 'BOOKS_FAILED';

// cart
export const BOOK_ADDED_TO_CART = 'BOOK_ADDED_TO_CART';

// offers
export const OFFERS_REQUESTED = 'OFFERS_REQUESTED';
export const OFFERS_SUCCEEDED = 'OFFERS_SUCCEEDED';
export const OFFERS_FAILED = 'OFFERS_FAILED';
