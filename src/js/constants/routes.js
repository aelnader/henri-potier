/**
 * This file contains string constants for url routes
 */
export const BOOKS_ROUTE = '/livres';
export const CART_ROUTE = '/panier';
