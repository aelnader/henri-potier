/**
 * This file contains string constants for api calls
 */
export const POTTER_BOOKS_BASE_URL = 'http://henri-potier.xebia.fr';
// books
export const GET_BOOKS_URL = `${POTTER_BOOKS_BASE_URL}/books`;
// offers
export const GET_OFFERS = '/commercialOffers';
