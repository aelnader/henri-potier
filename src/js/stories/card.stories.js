import React from 'react';
import { storiesOf } from '@storybook/react'; // eslint-disable-line

import Card from '../components/presentational/Card';

const book = {
  isbn: 'c8fabf68-8374-48fe-a7ea-a00ccd07afff',
  title: "Henri Potier à l'école des sorciers",
  price: 35,
  cover: 'http://henri-potier.xebia.fr/hp0.jpg',
  synopsis: ['synopsis'],
};

storiesOf('Card', module)
  .add('Fixed Header with links and a drawer', () => (
    <div>
      <Card
        title={book.title}
        price={book.price}
        description={book.synopsis}
        pic={book.cover}
      />
    </div>
  ));
