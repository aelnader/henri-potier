import React from 'react';
import { storiesOf } from '@storybook/react'; // eslint-disable-line

import Badge from '../components/presentational/Badge';

const text = 'My Text';
const value = '2';

storiesOf('Badge', module)
  .add('Text with badge', () => (
    <div>
      <br />
      <Badge
        text={text}
        badgeValue={value}
      />
    </div>
  ));
