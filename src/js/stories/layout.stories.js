import React from 'react';
import { storiesOf } from '@storybook/react'; // eslint-disable-line

import Layout from '../components/container/Layout';

const title = 'Title';
const pages = [
  {
    id: 'page1',
    title: 'page1',
  },
  {
    id: 'page2',
    title: 'page2',
  },
  {
    id: 'page3',
    title: 'page3',
  },
];

storiesOf('Layout', module)
  .add('Fixed Header with links and a drawer', () => (
    <div>
      <Layout
        title={title}
        pages={pages}
      />
    </div>
  ));
