import * as actions from '../books';
import * as types from '../../constants/actionTypes';

describe('books actions', () => {
  it('should create an action to request books', () => {
    const expectedAction = {
      type: types.BOOKS_REQUESTED,
    };
    expect(actions.booksRequested()).toEqual(expectedAction);
  });
  it('should create an action to signal a succesful books fetch', () => {
    const payload =
      [
        {
          isbn: 'c8fabf68-8374-48fe-a7ea-a00ccd07afff',
          title: "Henri Potier à l'école des sorciers",
          price: 35,
          cover: 'http://henri-potier.xebia.fr/hp0.jpg',
          synopsis: ['synopsis'],
        },
      ];
    const expectedAction = {
      type: types.BOOKS_SUCCEEDED,
      payload,
    };
    expect(actions.booksSucceeded(payload)).toEqual(expectedAction);
  });
  it('should create an action to signal a failed books fetch', () => {
    const payload = {
      code: '404',
      message: 'Page not found',
    };
    const expectedAction = {
      type: types.BOOKS_FAILED,
      payload,
      error: true,
    };
    expect(actions.booksFailed(payload)).toEqual(expectedAction);
  });
});
