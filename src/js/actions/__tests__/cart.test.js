import * as actions from '../cart';
import * as types from '../../constants/actionTypes';

describe('cart actions', () => {
  it('should create an action to add book to cart', () => {
    const payload =
      {
        isbn: 'c8fabf68-8374-48fe-a7ea-a00ccd07afff',
      };
    const expectedAction = {
      type: types.BOOK_ADDED_TO_CART,
      payload,
    };
    expect(actions.bookAddedToCart(payload)).toEqual(expectedAction);
  });
  it('should create an action to signal an offers fetch request', () => {
    const payload = [
      {
        isbn: 'c8fabf68-8374-48fe-a7ea-a00ccd07afff',
        title: "Henri Potier à l'école des sorciers",
        price: 35,
        quantity: 1,
      }, {
        isbn: 'a460afed-e5e7-4e39-a39d-c885c05db861',
        title: 'Henri Potier et la Chambre des secrets',
        price: 25,
        quantity: 2,
      },
    ];
    const expectedAction = {
      type: types.OFFERS_REQUESTED,
      payload,
    };
    expect(actions.offersRequested(payload)).toEqual(expectedAction);
  });
  it('should create an action to signal a succesful offers fetch', () => {
    const payload = { bestOffer: 15 };
    const expectedAction = {
      type: types.OFFERS_SUCCEEDED,
      payload,
    };
    expect(actions.offersSucceeded(payload)).toEqual(expectedAction);
  });
  it('should create an action to signal a failed offers fetch', () => {
    const payload = {
      code: '404',
      message: 'Page not found',
    };
    const expectedAction = {
      type: types.OFFERS_FAILED,
      payload,
      error: true,
    };
    expect(actions.offersFailed(payload)).toEqual(expectedAction);
  });
});
