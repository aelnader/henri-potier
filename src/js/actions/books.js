import { BOOKS_REQUESTED, BOOKS_SUCCEEDED, BOOKS_FAILED } from '../constants/actionTypes';

/**
 * All actions related to books
 */
export function booksRequested() {
  return {
    type: BOOKS_REQUESTED,
  };
}
export function booksSucceeded(payload) {
  return {
    type: BOOKS_SUCCEEDED,
    payload,
  };
}
export function booksFailed(payload) {
  return {
    type: BOOKS_FAILED,
    payload,
    error: true,
  };
}
