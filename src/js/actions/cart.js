import { BOOK_ADDED_TO_CART, OFFERS_REQUESTED, OFFERS_SUCCEEDED, OFFERS_FAILED } from '../constants/actionTypes';

/**
 * All actions related to cart
 */

export function bookAddedToCart(payload) {
  return {
    type: BOOK_ADDED_TO_CART,
    payload,
  };
}

export function offersRequested(payload) {
  return {
    type: OFFERS_REQUESTED,
    payload,
  };
}

export function offersSucceeded(payload) {
  return {
    type: OFFERS_SUCCEEDED,
    payload,
  };
}

export function offersFailed(payload) {
  return {
    type: OFFERS_FAILED,
    payload,
    error: true,
  };
}
