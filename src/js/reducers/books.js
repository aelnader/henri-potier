import { BOOKS_REQUESTED, BOOKS_SUCCEEDED, BOOKS_FAILED } from '../constants/actionTypes';
/**
 * Handles book actions
 */
const initialState = {
  booksList: [],
  fetchingBooks: false,
  error: {},
};
const books = (state = initialState, action) => {
  switch (action.type) {
    case BOOKS_REQUESTED:
      return Object.assign({}, state, {
        fetchingBooks: true,
      });
    case BOOKS_SUCCEEDED:
      return {
        fetchingBooks: false,
        error: {},
        booksList: action.payload,
      };
    case BOOKS_FAILED:
      return Object.assign({}, state, {
        fetchingBooks: false,
        error: action.payload,
      });
    default:
      return state;
  }
};

export default books;
