import { BOOK_ADDED_TO_CART, OFFERS_REQUESTED, OFFERS_SUCCEEDED, OFFERS_FAILED } from '../constants/actionTypes';
/**
 * Handles cart actions
 */
const initialState = {
  items: [],
  bestOffer: 0,
  pendingOffers: false,
};

const cart = (state = initialState, action) => {
  switch (action.type) {
    case BOOK_ADDED_TO_CART: {
      const bookIndex = state.items.findIndex(element => element.isbn === action.payload.isbn);
      if (bookIndex >= 0) {
        // book already exists in cart
        const newArray = state.items.map((item, index) => {
          if (index !== bookIndex) {
            // This isn't the item we care about - keep it as-is
            return item;
          }
          // Otherwise, this is the one we want - return an updated value
          return {
            isbn: item.isbn,
            quantity: item.quantity + 1,
          };
        });
        return {
          items: newArray,
          pendingOffers: false,
          bestOffer: 0,
        };
      }
      // book does not already exist in cart
      const newArray = state.items.slice();
      newArray.splice(state.items.length, 0, { isbn: action.payload.isbn, quantity: 1 });
      return {
        items: newArray,
        pendingOffers: false,
        bestOffer: 0,
      };
    }
    case OFFERS_REQUESTED:
      return Object.assign({}, state, {
        pendingOffers: true,
      });
    case OFFERS_SUCCEEDED:
      return Object.assign({}, state, {
        bestOffer: action.payload.bestOffer,
        pendingOffers: false,
      });
    case OFFERS_FAILED:
      return Object.assign({}, state, {
        bestOffer: 0,
        pendingOffers: false,
      });
    default:
      return state;
  }
};

export default cart;
