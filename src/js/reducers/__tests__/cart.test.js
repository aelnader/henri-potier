import cart from '../cart';
import * as actionTypes from '../../constants/actionTypes';

describe('cart reducer', () => {
  it('should return the initial state', () => {
    expect(cart(undefined, {})).toEqual({
      items: [],
      bestOffer: 0,
      pendingOffers: false,
    });
  });
  it('should handle BOOK_ADDED_TO_CART', () => {
    const state = {
      items: [],
      bestOffer: 0,
      pendingOffers: false,
    };
    expect(cart(state, {
      type: actionTypes.BOOK_ADDED_TO_CART,
      payload: {
        isbn: 'c8fabf68-8374-48fe-a7ea-a00ccd07afff',
      },
    })).toEqual({
      items: [{ isbn: 'c8fabf68-8374-48fe-a7ea-a00ccd07afff', quantity: 1 }],
      bestOffer: 0,
      pendingOffers: false,
    });
  });
  it('should handle quantity if same book is added multiple times to cart', () => {
    const state = {
      items: [{ isbn: 'c8fabf68-8374-48fe-a7ea-a00ccd07afff', quantity: 2 }],
      bestOffer: 0,
    };
    expect(cart(state, {
      type: actionTypes.BOOK_ADDED_TO_CART,
      payload: {
        isbn: 'c8fabf68-8374-48fe-a7ea-a00ccd07afff',
      },
    })).toEqual({
      items: [{ isbn: 'c8fabf68-8374-48fe-a7ea-a00ccd07afff', quantity: 3 }],
      bestOffer: 0,
      pendingOffers: false,
    });
  });
  it('should handle OFFERS_REQUESTED', () => {
    const state = {
      items: [{ isbn: 'c30968db-cb1d-442e-ad0f-80e37c077f89', quantity: 1 }],
      bestOffer: 0,
    };
    expect(cart(state, {
      type: actionTypes.OFFERS_REQUESTED,
    })).toEqual({
      items: [{ isbn: 'c30968db-cb1d-442e-ad0f-80e37c077f89', quantity: 1 }],
      bestOffer: 0,
      pendingOffers: true,
    });
  });
  it('should handle OFFERS_SUCCEEDED', () => {
    const state = {
      items: [{ isbn: 'c30968db-cb1d-442e-ad0f-80e37c077f89', quantity: 1 }],
      bestOffer: 0,
    };
    expect(cart(state, {
      type: actionTypes.OFFERS_SUCCEEDED,
      payload: { bestOffer: 15 },
    })).toEqual({
      items: [{ isbn: 'c30968db-cb1d-442e-ad0f-80e37c077f89', quantity: 1 }],
      bestOffer: 15,
      pendingOffers: false,
    });
  });
  it('should handle OFFERS_FAILED', () => {
    const state = {
      items: [{ isbn: 'c30968db-cb1d-442e-ad0f-80e37c077f89', quantity: 1 }],
      bestOffer: 15,
    };
    expect(cart(state, {
      type: actionTypes.OFFERS_FAILED,
      payload: {
        code: '404',
        message: 'Page not found',
      },
    })).toEqual({
      items: [{ isbn: 'c30968db-cb1d-442e-ad0f-80e37c077f89', quantity: 1 }],
      bestOffer: 0,
      pendingOffers: false,
    });
  });
});
