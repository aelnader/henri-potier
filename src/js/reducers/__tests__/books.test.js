import books from '../books';
import * as actionTypes from '../../constants/actionTypes';

describe('books reducer', () => {
  it('should return the initial state', () => {
    expect(books(undefined, {})).toEqual({
      booksList: [],
      fetchingBooks: false,
      error: {},
    });
  });
  it('should handle BOOKS_REQUESTED', () => {
    const state = {
      booksList: [],
      fetchingBooks: false,
      error: {},
    };
    expect(books(state, {
      type: actionTypes.BOOKS_REQUESTED,
    })).toEqual({
      booksList: [],
      fetchingBooks: true,
      error: {},
    });
  });
  it('should handle BOOKS_SUCCEEDED', () => {
    const state = {
      booksList: [],
      fetchingBooks: true,
      error: {},
    };
    expect(books(state, {
      type: actionTypes.BOOKS_SUCCEEDED,
      payload: [
        {
          isbn: 'c8fabf68-8374-48fe-a7ea-a00ccd07afff',
          title: "Henri Potier à l'école des sorciers",
          price: 35,
          cover: 'http://henri-potier.xebia.fr/hp0.jpg',
          synopsis: ['synopsis'],
        },
      ],
    })).toEqual({
      booksList: [
        {
          isbn: 'c8fabf68-8374-48fe-a7ea-a00ccd07afff',
          title: "Henri Potier à l'école des sorciers",
          price: 35,
          cover: 'http://henri-potier.xebia.fr/hp0.jpg',
          synopsis: ['synopsis'],
        },
      ],
      fetchingBooks: false,
      error: {},
    });
  });
  it('should handle BOOKS_FAILED', () => {
    const state = {
      booksList: [],
      fetchingBooks: true,
      error: {},
    };
    expect(books(state, {
      type: actionTypes.BOOKS_FAILED,
      payload: {
        code: '404',
        message: 'Page not found',
      },
    })).toEqual({
      booksList: [],
      fetchingBooks: false,
      error: {
        code: '404',
        message: 'Page not found',
      },
    });
  });
});
