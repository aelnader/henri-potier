import { combineReducers } from 'redux';
import books from './books';
import cart from './cart';

const potterBooksApp = combineReducers({
  books,
  cart,
});

export default potterBooksApp;
