import { connect } from 'react-redux';
import React from 'react';
import PropTypes from 'prop-types';
// actions
import { offersRequested } from '../../actions/cart';
// constants
import { CART_EMPTY_MSG } from '../../constants/wordings';
/**
 * Container/Page for showing what's in customuer's cart
 * @type Cart
 */
export class Cart extends React.Component {
  constructor() {
    super();
    this.state = {};
  }
  componentWillMount() {
    this.props.dispatchOffersRequested(this.props.itemsWithPrices);
  }
  render() {
    const { itemsWithPrices, bestOffer } = this.props;
    if (itemsWithPrices.length === 0) {
      // cart is empty
      return (
        <div className="empty-cart-msg">{CART_EMPTY_MSG}</div>
      );
    }
    // calculate total price to show
    let totalPrice = 0;
    for (let i = 0; i < itemsWithPrices.length; i += 1) {
      totalPrice += (itemsWithPrices[i].price * itemsWithPrices[i].quantity);
    }
    // map item rows to show in cart
    const itemRows = itemsWithPrices.map(item =>
      (
        <tr key={item.isbn} className="item-detail-row">
          <td className="mdl-data-table__cell--non-numeric">{item.title}</td>
          <td>{item.quantity}</td>
          <td>{item.price * item.quantity}€</td>
        </tr>
      ));
    return (
      <table className="mdl-data-table mdl-js-data-table mdl-shadow--2dp cart-table">
        <thead>
          <tr>
            <th className="mdl-data-table__cell--non-numeric">Livre</th>
            <th>Quantité</th>
            <th>Prix</th>
          </tr>
        </thead>
        <tbody>
          {itemRows}
          <tr className="best-offer-row">
            <td className="mdl-data-table__cell--non-numeric">Offre</td>
            <td />
            <td>{-bestOffer}€</td>
          </tr>
          <tr className="total-price-row">
            <td className="mdl-data-table__cell--non-numeric">Prix Total</td>
            <td />
            <td>{totalPrice - bestOffer}€</td>
          </tr>
        </tbody>
      </table>
    );
  }
}

Cart.propTypes = {
  itemsWithPrices: PropTypes.array.isRequired,
  dispatchOffersRequested: PropTypes.func,
  bestOffer: PropTypes.number,
};
Cart.defaultProps = {
  dispatchOffersRequested: () => {},
  bestOffer: 0,
};

function mapStateToProps(state) {
  const { items } = state.cart;
  const { booksList } = state.books;
  const itemsWithPrices = [];
  for (let i = 0; i < items.length; i += 1) {
    const book = booksList.find(element => element.isbn === items[i].isbn);
    itemsWithPrices.push({
      isbn: items[i].isbn,
      quantity: items[i].quantity,
      title: book.title,
      price: book.price,
    });
  }
  const mappedProps = {
    itemsWithPrices,
    bestOffer: state.cart.bestOffer,
  };
  return mappedProps;
}
function mapDispatchToProps(dispatch) {
  return ({
    dispatchOffersRequested: (payload) => { dispatch(offersRequested(payload)); },
  });
}

export default connect(mapStateToProps, mapDispatchToProps)(Cart);
