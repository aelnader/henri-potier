import React from 'react';
import { Route, Switch } from 'react-router-dom';
import PropTypes from 'prop-types';

// components
import BooksShowcase from './BooksShowcase'; // eslint-disable-line
import Cart from './Cart'; // eslint-disable-line
import Header from '../presentational/Header';
import Drawer from '../presentational/Drawer';
// constants
import { BOOKS_ROUTE, CART_ROUTE } from '../../constants/routes';

/**
 * Layout defines the main layout for the website containing a header and drawer
 * @param {string} title is the main title of the website
 * @param {array} pages array of page objects containing info about each page(id, name, link).
 */
const Layout = ({ title, pages }) => (
  <div className="mdl-layout mdl-js-layout mdl-layout--fixed-header">
    <Header title={title} pages={pages} />
    <Drawer title={title} pages={pages} />
    <main className="mdl-layout__content main">
      <div className="page-content">
        <Switch>
          <Route exact path="/" component={BooksShowcase} />
          <Route path={BOOKS_ROUTE} component={BooksShowcase} />
          <Route path={CART_ROUTE} component={Cart} />
          <Route exact path="*" component={BooksShowcase} />
        </Switch>
      </div>
    </main>
  </div>
);

Layout.propTypes = {
  title: PropTypes.string.isRequired,
  pages: PropTypes.array.isRequired,
};

export default Layout;
