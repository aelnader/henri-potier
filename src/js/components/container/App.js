import { connect } from 'react-redux';
import React from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
// action
import { booksRequested } from '../../actions/books';
// components
import Layout from './Layout';
// constants
import { MAIN_TITLE, PAGE_BOOKS_TITLE, PAGE_CART_TITLE } from '../../constants/wordings';
import { BOOKS_ROUTE, CART_ROUTE } from '../../constants/routes';

/**
 * Main component, everything renders inside App
 */
class App extends React.Component {
  constructor() {
    super();
    this.state = {
      layoutConfig: {
        title: MAIN_TITLE,
        pages: [
          {
            id: `${PAGE_BOOKS_TITLE}${1}`,
            title: PAGE_BOOKS_TITLE,
            route: BOOKS_ROUTE,
          },
          {
            id: `${PAGE_CART_TITLE}${2}`,
            title: PAGE_CART_TITLE,
            route: CART_ROUTE,
          },
        ],
      },
    };
  }
  componentWillMount() {
    this.props.dispatchBooksRequested();
  }
  componentWillReceiveProps(props) {
    const newConf = this.state.layoutConfig;
    const { numberOfSelectedBooks } = props;
    if (numberOfSelectedBooks > 0) {
      newConf.pages[1].badgeValue = `${numberOfSelectedBooks}`;
      this.setState({
        layoutConfig: newConf,
      });
    }
  }
  render() {
    const { layoutConfig } = this.state;
    return (
      <div>
        <Layout title={layoutConfig.title} pages={layoutConfig.pages} />
      </div>
    );
  }
}

function mapStateToProps(state) {
  // Calculate number of selected books to show in header
  let numberOfSelectedBooks = 0;
  const { items } = state.cart;
  for (let i = 0; i < items.length; i += 1) {
    numberOfSelectedBooks += items[i].quantity;
  }
  const mappedProps = {
    numberOfSelectedBooks,
  };
  return mappedProps;
}
function mapDispatchToProps(dispatch) {
  return ({
    dispatchBooksRequested: () => { dispatch(booksRequested()); },
  });
}

App.propTypes = {
  dispatchBooksRequested: PropTypes.func.isRequired,
  numberOfSelectedBooks: PropTypes.number.isRequired,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
