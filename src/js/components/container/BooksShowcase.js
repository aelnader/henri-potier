import { connect } from 'react-redux';
import React from 'react';
import PropTypes from 'prop-types';
// actions
import { bookAddedToCart } from '../../actions/cart';
// components
import Card from '../presentational/Card';
import ExpandingInput from '../presentational/ExpandingInput';
// constants
import { FETCH_BOOKS_ERROR_MSG } from '../../constants/wordings';

/**
 * Container/Page for showing and searching books
 * @param {array} books books list to show, each book has attr: isbn, title, price, cover, synopsis.
 * @param {object} error the error in fetching books, has attr: code, msg
 */
export class BooksShowcase extends React.Component {
  constructor() {
    super();
    this.state = { displayedBooks: [] };

    // bindings
    this.onAddBook = this.onAddBook.bind(this);
    this.onSearchChanged = this.onSearchChanged.bind(this);
  }
  componentWillMount() {
    this.setState({
      displayedBooks: this.props.books,
    });
  }
  componentWillReceiveProps(props) {
    if (this.state.displayedBooks && this.state.displayedBooks.length === 0) {
      this.setState({
        displayedBooks: props.books,
      });
    }
  }
  onAddBook(isbn) {
    const payload = {
      isbn,
    };
    this.props.dispatchBookAddedToCart(payload);
  }
  onSearchChanged(value) {
    if (value) {
      const filteredBooks = this.props.books
        .filter(book => book.title.toLowerCase()
          .includes(value.toLowerCase()));
      this.setState({
        displayedBooks: filteredBooks,
      });
    } else {
      this.setState({
        displayedBooks: this.props.books,
      });
    }
  }
  render() {
    const { error, books } = this.props;
    const { displayedBooks } = this.state;
    if (books.length === 0) {
      if (error.code) {
        // fetching books failed
        return (<h1>{FETCH_BOOKS_ERROR_MSG}</h1>);
      }
      // fetching books in progress
      return (<h1>Loading</h1>);
    }
    const bookcards = displayedBooks.map(book =>
      (
        <Card
          title={book.title}
          price={book.price}
          description={book.synopsis}
          pic={book.cover}
          key={book.isbn}
          isbn={book.isbn}
          onBookSelect={this.onAddBook}
        />
      ));
    return (
      <div>
        <form><ExpandingInput onInputChange={this.onSearchChanged} /></form>
        <div className="cards-holder">{ bookcards }</div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const mappedProps = {
    books: state.books.booksList,
    error: state.books.error,
  };
  return mappedProps;
}
function mapDispatchToProps(dispatch) {
  return ({
    dispatchBookAddedToCart: (payload) => { dispatch(bookAddedToCart(payload)); },
  });
}

BooksShowcase.propTypes = {
  books: PropTypes.array,
  error: PropTypes.object,
  dispatchBookAddedToCart: PropTypes.func,
};
BooksShowcase.defaultProps = {
  books: [],
  error: {},
  dispatchBookAddedToCart: () => {},
};
export default connect(mapStateToProps, mapDispatchToProps)(BooksShowcase);
