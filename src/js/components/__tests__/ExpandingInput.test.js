import React from 'react';
import { shallow } from 'enzyme';

import ExpandingInput from '../presentational/ExpandingInput';

let wrapper;
let inputChangeFn;
let labelClickedFn;
beforeEach(() => {
  inputChangeFn = jest.fn();
  labelClickedFn = jest.fn();
  wrapper = shallow(<ExpandingInput onInputChange={inputChangeFn} />);
});
afterEach(() => {
  inputChangeFn.mockReset();
  labelClickedFn.mockReset();
});
describe('ExpandingInput', () => {
  it('should show an input field.', () => {
    expect(wrapper.find('input').length).toEqual(1);
  });
  it('should show an input that is expandable.', () => {
    expect(wrapper.find('.mdl-textfield--expandable').length).toEqual(1);
  });
  it('should call function when input value is changed', () => {
    const input = wrapper.find('input').first();
    const mockedEvent = { target: { value: 'test' } };
    input.simulate('change', mockedEvent);
    // Check that the function has been called
    expect(inputChangeFn.mock.calls.length).toEqual(1);
  });
});
