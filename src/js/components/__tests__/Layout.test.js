import React from 'react';
import { shallow } from 'enzyme';

import Drawer from '../presentational/Drawer';
import Header from '../presentational/Header';
import Layout from '../container/Layout';

let layoutConfig;
let wrapperLayout;
let wrapperHeader;
let wrapperDrawer;

beforeEach(() => {
  // This is run before every test
  layoutConfig = {
    title: 'My Title for this page',
    pages: [
      {
        id: 'page1',
        title: 'page1',
        route: '/route1',
      },
      {
        id: 'page2',
        title: 'page2',
        route: '/route2',
      },
      {
        id: 'page3',
        title: 'page3',
        route: '/route3',
      },
    ],
  };
  wrapperLayout = shallow(<Layout title={layoutConfig.title} pages={layoutConfig.pages} />);
  wrapperHeader = shallow(<Header title={layoutConfig.title} pages={layoutConfig.pages} />);
  wrapperDrawer = shallow(<Drawer title={layoutConfig.title} pages={layoutConfig.pages} />);
});
describe('Layout', () => {
  it('should have a header and a drawer', () => {
    expect(wrapperLayout.contains([<Header title={layoutConfig.title} pages={layoutConfig.pages} />,
      <Drawer title={layoutConfig.title} pages={layoutConfig.pages} />])).toBeTruthy();
  });
  it('should show title in header and drawer', () => {
    expect(wrapperHeader.text().includes(layoutConfig.title)).toBeTruthy();
    expect(wrapperDrawer.text().includes(layoutConfig.title)).toBeTruthy();
  });
  it('should have a header with a number of tabs equal to number of pages on the website', () => {
    expect(wrapperHeader.find('Link').length).toEqual(layoutConfig.pages.length);
  });
  it('should have a header and drawer with same number of tabs', () => {
    expect(wrapperHeader.find('Link').length).toEqual(wrapperDrawer.find('Link').length);
  });
});
