import React from 'react';
import { mount } from 'enzyme';

import { BooksShowcase } from '../container/BooksShowcase';
import Card from '../presentational/Card';
import { FETCH_BOOKS_ERROR_MSG } from '../../constants/wordings';

let books;
beforeEach(() => {
  books = [
    {
      isbn: 'c8fabf68-8374-48fe-a7ea-a00ccd07afff',
      title: "Henri Potier à l'école des sorciers",
      price: 35,
      cover: 'http://henri-potier.xebia.fr/hp0.jpg',
      synopsis: ['synopsis1'],
    },
    {
      isbn: 'a460afed-e5e7-4e39-a39d-c885c05db861',
      title: 'Henri Potier et la Chambre des secrets',
      price: 30,
      cover: 'http://henri-potier.xebia.fr/hp1.jpg',
      synopsis: ['synopsis2'],
    },
  ];
});
describe('BooksShowcase', () => {
  it('should render a card for each book', () => {
    const error = {};
    const wrapper = mount(<BooksShowcase books={books} error={error} />);
    expect(wrapper.find(Card)).toHaveLength(books.length);
  });
  it('should filter books depending on user input', () => {
    const error = {};
    const wrapper = mount(<BooksShowcase books={books} error={error} />);
    wrapper.instance().onSearchChanged('secrets');
    wrapper.update();
    expect(wrapper.find(Card)).toHaveLength(1);
  });
  it('should show all books if search is empty string', () => {
    const error = {};
    const wrapper = mount(<BooksShowcase books={books} error={error} />);
    wrapper.instance().onSearchChanged('secrets');
    wrapper.instance().onSearchChanged('');
    wrapper.update();
    expect(wrapper.find(Card)).toHaveLength(2);
  });
  it('should show no books if no books are found', () => {
    const error = {};
    const wrapper = mount(<BooksShowcase books={books} error={error} />);
    wrapper.instance().onSearchChanged('bla bla');
    wrapper.update();
    expect(wrapper.find(Card)).toHaveLength(0);
  });
  it('should show an error message if books fetch failed', () => {
    const error = {
      code: 404,
      message: 'Page not found',
    };
    const wrapper = mount(<BooksShowcase books={[]} error={error} />);
    expect(wrapper.text()).toEqual(FETCH_BOOKS_ERROR_MSG);
  });
});
