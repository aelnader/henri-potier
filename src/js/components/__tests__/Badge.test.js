import React from 'react';
import { shallow } from 'enzyme';

import Badge from '../presentational/Badge';

let text;

beforeEach(() => {
  text = 'Amazing text';
});
describe('Badge', () => {
  it('should show a badge with a text and value if a value is defined.', () => {
    const value = '2';
    const wrapper = shallow(<Badge
      text={text}
      badgeValue={value}
    />);
    expect(wrapper.contains(<span className="mdl-badge" data-badge={value}>{text}</span>)).toBeTruthy();
  });
  it('should just return text if no badge value is given.', () => {
    const wrapper = shallow(<Badge
      text={text}
    />);
    expect(wrapper.equals(<span>{text}</span>)).toBeTruthy();
  });
});
