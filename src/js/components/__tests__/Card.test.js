import React from 'react';
import { shallow } from 'enzyme';

import Card from '../presentational/Card';

let book;
let wrapper;
let bookSelectFn;

beforeEach(() => {
  book = {
    isbn: 'c8fabf68-8374-48fe-a7ea-a00ccd07afff',
    title: "Henri Potier à l'école des sorciers",
    price: 35,
    cover: 'http://henri-potier.xebia.fr/hp0.jpg',
    synopsis: ['synopsis'],
  };
  bookSelectFn = jest.fn();
  wrapper = shallow(<Card
    title={book.title}
    price={book.price}
    description={book.synopsis}
    pic={book.cover}
    isbn={book.isbn}
    onBookSelect={bookSelectFn}
  />);
});
afterEach(() => {
  bookSelectFn.mockReset();
});
describe('Card', () => {
  it('should show a title and price.', () => {
    expect(wrapper.contains(<h2 className="mdl-card__title-text">{`${book.title} / ${book.price}€`}</h2>)).toBeTruthy();
  });
  it('should show descriptions', () => {
    expect(wrapper.contains(<div className="mdl-card__supporting-text">{ book.synopsis[0].substring(0, 110) }...  <a href="#">lire la suite</a></div>)).toBeTruthy();
  });
  it('should show a link to add book to cart', () => {
    expect(wrapper.find('.mdl-button').length).toEqual(1);
  });
  it('should call function to add book to cart when add book is clicked', () => {
    const addCartButton = wrapper.find('.mdl-button').first();
    addCartButton.simulate('click');
    // Check that the function has been called
    expect(bookSelectFn.mock.calls.length).toEqual(1);
    // Check it's been called with the right arguments
    expect(bookSelectFn.mock.calls[0][0]).toEqual(book.isbn, book.price);
  });
});
