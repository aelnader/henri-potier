import React from 'react';
import { shallow } from 'enzyme';

import { Cart } from '../container/Cart';

let itemsWithPrices;
let wrapper;

beforeEach(() => {
  itemsWithPrices = [
    {
      isbn: 'c8fabf68-8374-48fe-a7ea-a00ccd07afff',
      title: "Henri Potier à l'école des sorciers",
      price: 35,
      quantity: 1,
    }, {
      isbn: 'a460afed-e5e7-4e39-a39d-c885c05db861',
      title: 'Henri Potier et la Chambre des secrets',
      price: 25,
      quantity: 2,
    },
  ];
  wrapper = shallow(<Cart
    itemsWithPrices={itemsWithPrices}
  />);
});
describe('Card', () => {
  it('should show a table containing cart details.', () => {
    expect(wrapper.find('table').length).toEqual(1);
  });
  it('should show a rows with quantity and price for books in the cart', () => {
    const detailsRow =
    (
      <tr className="item-detail-row">
        <td className="mdl-data-table__cell--non-numeric">{itemsWithPrices[1].title}</td>
        <td>{itemsWithPrices[1].quantity}</td>
        <td>{itemsWithPrices[1].price * itemsWithPrices[1].quantity}€</td>
      </tr>
    );
    expect(wrapper.contains(detailsRow)).toBeTruthy();
  });
  it('should show as many details rows as there are books in the cart', () => {
    expect(wrapper.find('.item-detail-row').length).toEqual(itemsWithPrices.length);
  });
  it('should show a row with offer applied', () => {
    expect(wrapper.find('.best-offer-row').length).toEqual(1);
  });
  it('should show a row with total price', () => {
    expect(wrapper.find('.total-price-row').length).toEqual(1);
  });
  it('should show a message if cart is Empty', () => {
    const EmptyitemsWithPrices = [];
    const emptyWrapper = shallow(<Cart
      itemsWithPrices={EmptyitemsWithPrices}
    />);
    expect(emptyWrapper.find('.empty-cart-msg').length).toEqual(1);
  });
});
