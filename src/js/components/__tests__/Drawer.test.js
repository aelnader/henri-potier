import React from 'react';
import { Link } from 'react-router-dom';
import { shallow } from 'enzyme';

import Drawer from '../presentational/Header';
import Badge from '../presentational/Badge';

let layoutConfig;
let wrapperDrawer;

beforeEach(() => {
  // This is run before every test
  layoutConfig = {
    title: 'My Title for this page',
    pages: [
      {
        id: 'page1',
        title: 'page1',
        route: '/route1',
      },
      {
        id: 'page2',
        title: 'page2',
        route: '/route2',
        badgeValue: '2',
      },
      {
        id: 'page3',
        title: 'page3',
        route: '/route3',
      },
    ],
  };
  wrapperDrawer = shallow(<Drawer title={layoutConfig.title} pages={layoutConfig.pages} />);
});
describe('Drawer', () => {
  it('should have links to each page on the website', () => {
    expect(wrapperDrawer.contains([<Link to={layoutConfig.pages[0].route} className="mdl-navigation__link" href key={layoutConfig.pages[0].id}><Badge text={layoutConfig.pages[0].title} badgeValue={layoutConfig.pages[0].badgeValue} /></Link>,
      <Link to={layoutConfig.pages[1].route} className="mdl-navigation__link" href key={layoutConfig.pages[1].id}><Badge text={layoutConfig.pages[1].title} badgeValue={layoutConfig.pages[1].badgeValue} /></Link>,
      <Link to={layoutConfig.pages[2].route} className="mdl-navigation__link" href key={layoutConfig.pages[2].id}><Badge text={layoutConfig.pages[2].title} badgeValue={layoutConfig.pages[2].badgeValue} /></Link>])).toBeTruthy();
  });
});
