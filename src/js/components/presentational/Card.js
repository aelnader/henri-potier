import PropTypes from 'prop-types';
import React from 'react';
// constants
import { ADD_TO_CART_BUTTON } from '../../constants/wordings';

/**
 * Component for showcasing a book
 * @param {string} title        book title
 * @param {number} price        book price
 * @param {string} description  book synopsis
 * @param {string} pic          book cover url
 * @param {string} isbn         book isbn
 * @param {func} onBookSelect   function to call when book is selected
 */
const Card = ({
  title,
  price,
  description,
  pic,
  isbn,
  onBookSelect,
}) => {
  const divStylePic = {
    color: 'white',
    background: `url('${pic}') bottom right 15% no-repeat #FFC107`,
    backgroundSize: 'contain',
  };
  const divStyleMesures = {
    width: '320px',
    height: '620px',
  };
  return (
    <div className="card-parent">
      <div className="demo-card-square mdl-card mdl-shadow--2dp card" style={divStyleMesures}>
        <div className="mdl-card__title mdl-card--expand" style={divStylePic}>
          <h2 className="mdl-card__title-text">{`${title} / ${price}€`}</h2>
        </div>
        <div className="mdl-card__supporting-text">
          { description[0].substring(0, 110) }...  <a href="#">lire la suite</a>
        </div>
        <div className="mdl-card__actions mdl-card--border">
          <button onClick={() => onBookSelect(isbn)} onKeyPress={() => onBookSelect(isbn)} className="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">
            { ADD_TO_CART_BUTTON }
          </button>
        </div>
      </div>
    </div>
  );
};

Card.propTypes = {
  title: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  description: PropTypes.array.isRequired,
  pic: PropTypes.string.isRequired,
  isbn: PropTypes.string.isRequired,
  onBookSelect: PropTypes.func.isRequired,
};
export default Card;
