import React from 'react';
import PropTypes from 'prop-types';
/**
 * Show a badge atop of some text
 * @param {text} text       text atop which badge is shown
 * @param {badgeValue} badgeValue badge to show
 */
const Badge = ({ text, badgeValue }) => {
  if (badgeValue) {
    return (
      <span className="mdl-badge" data-badge={badgeValue}>{text}</span>
    );
  }
  return (
    <span>{text}</span>
  );
};

Badge.propTypes = {
  text: PropTypes.string.isRequired,
  badgeValue: PropTypes.string,
};
Badge.defaultProps = {
  badgeValue: '',
};
export default Badge;
