import React from 'react';
import PropTypes from 'prop-types';

/**
 * Input that exapand on click
 */
class ExpandingInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = { value: '', toggle: false };
    // bindings
    this.handleChange = this.handleChange.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.handleBlur = this.handleBlur.bind(this);
  }
  handleChange(event) {
    const { value } = event.target;
    this.setState({ value });
    this.props.onInputChange(value);
  }
  handleClick() {
    this.setState({ toggle: !this.state.toggle });
  }
  handleBlur() {
    if (!this.state.value) {
      this.setState({ toggle: false });
    }
  }
  render() {
    let expandedClass = '';
    if (this.state.toggle) {
      expandedClass = 'is-focused';
    }
    return (
      <div className={`mdl-textfield mdl-js-textfield mdl-textfield--expandable expanding-input ${expandedClass}`}>
        <label className="mdl-button mdl-js-button mdl-button--icon" htmlFor="search-books-input" onClick={this.handleClick} onKeyPress={this.handleClick}>{// eslint-disable-line
        }
          <i className="material-icons">search</i>
        </label>
        <div className="mdl-textfield__expandable-holder">
          <input className="mdl-textfield__input" type="text" id="search-books-input" value={this.state.value} onChange={this.handleChange} onBlur={this.handleBlur} />
        </div>
      </div>
    );
  }
}

ExpandingInput.propTypes = {
  onInputChange: PropTypes.func,
};
ExpandingInput.defaultProps = {
  onInputChange: () => {},
};
export default ExpandingInput;
